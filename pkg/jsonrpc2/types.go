package jsonrpc2

type MsgBase struct {
	JSONRPC string `json:"jsonrpc"`
	Id      int64  `json:"id"`
}

type RequestBase struct {
	MsgBase
	Method  string      `json:"method"`
}

type RequestNull RequestBase

// These two types are placeholders for the test/example marshaling
type RequestObject struct {
	RequestBase
	Params interface{} `json:"params"`
}
type RequestSlice struct {
	RequestBase
	Params  []interface{} `json:"params"`
}

type Context struct {
	Level      string `json:"level"`
	File       string `json:"file"`
	Line       int    `json:"line"`
	Method     string `json:"method"`
	Hostname   string `json:"hostname"`
	ThreadName string `json:"thread_name"`
	Timestamp  string `json:"timestamp"` // format: "2017-06-07T02:28:07"
}

type Stack struct {
	Context Context     `json:"context"`
	Format  string      `json:"format"`
	Data    interface{} `json:"data"`
}

type Data struct {
	Code    int     `json:"code"`
	Name    string  `json:"name"`
	Message string  `json:"message"`
	Stack   []Stack `json:"stack"`
}

type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    Data   `json:"data"`
}

// The response type should be embedded into all concrete result types to get expected format of data
type Response struct {
	MsgBase
}

type ResponseNull Response

// These two types are placeholders for the examples,
// the concrete types to unmarshal to should switch on the existing Request with matching id
// If the error field is present the ResponseError type should be used
type ResponseSlice struct {
	Response
	Result []interface{} `json:"result"`
}
type ResponseObject struct {
	Response
	Result interface{} `json:"result"`
}

// This type should be selected if error field is present and discards the result if any by json tags
type ResponseError struct {
	Response
	Error Error `json:"error"`
	Result interface{} `json:"-"`
}
