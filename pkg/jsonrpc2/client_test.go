package jsonrpc2

import (
	"testing"
	"time"
)

var hostname ="ws://127.0.0.1:8752"

func TestNewClient(t *testing.T) {
	c := NewClient()
	if err := c.Dial(hostname); err != nil {
		t.Log(err)
		t.Log("no node reachable on '"+hostname+"'")
		t.Fail()
	}
	time.Sleep(time.Second*9)
	if err := c.Close(); err != nil {
		t.Log(err)
		t.Fail()
	}
}
