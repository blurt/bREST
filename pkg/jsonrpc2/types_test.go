package jsonrpc2

import (
	"encoding/json"
	"fmt"
	"os"
)

var msgBase = MsgBase{JSONRPC: "2.0", Id: 0}
var reqBase = RequestBase{Method: "", MsgBase: msgBase}
var resBase = Response{MsgBase: msgBase}

func ExampleRequestNull() {
	j, err := json.MarshalIndent(&RequestObject{
		RequestBase: reqBase,
	}, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("\n" + string(j))
	// Output:
	//
	// {
	//   "jsonrpc": "2.0",
	//   "id": 0,
	//   "method": "",
	//   "params": null
	// }
}

func ExampleRequestObject() {
	j, err := json.MarshalIndent(&RequestObject{
		RequestBase: reqBase,
		Params:      struct{}{},
	}, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("\n" + string(j))
	// Output:
	//
	// {
	//   "jsonrpc": "2.0",
	//   "id": 0,
	//   "method": "",
	//   "params": {}
	// }
}

func ExampleRequestSlice() {
	j, err := json.MarshalIndent(&RequestSlice{
		RequestBase: reqBase,
		Params:      []interface{}{},
	}, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("\n" + string(j))
	// Output:
	//
	// {
	//   "jsonrpc": "2.0",
	//   "id": 0,
	//   "method": "",
	//   "params": []
	// }
}

func ExampleResponseNull() {
	j, err := json.MarshalIndent(&Response{msgBase}, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("\n" + string(j))
	// Output:
	//
	// {
	//   "jsonrpc": "2.0",
	//   "id": 0
	// }
}

func ExampleResponseSlice() {
	j, err := json.MarshalIndent(&ResponseSlice{Response: resBase, Result: []interface{}{}}, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("\n" + string(j))
	// Output:
	//
	// {
	//   "jsonrpc": "2.0",
	//   "id": 0,
	//   "result": []
	// }
}

func ExampleResponseObject() {
	j, err := json.MarshalIndent(&ResponseObject{
		Response: resBase,
		Result:   struct{}{},
	}, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("\n" + string(j))
	// Output:
	//
	// {
	//   "jsonrpc": "2.0",
	//   "id": 0,
	//   "result": {}
	// }
}

func ExampleResponseError() {
	j, err := json.MarshalIndent(&ResponseError{
		Response: resBase,
		Error: Error{
			Code:    0,
			Message: "",
			Data: Data{
				Code:    0,
				Name:    "",
				Message: "",
				Stack: []Stack{
					{
						Context: Context{},
						Format:  "",
						Data:    nil,
					},
				},
			},
		},
	}, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("\n" + string(j))
	// Output:
	//
	// {
	//   "jsonrpc": "2.0",
	//   "id": 0,
	//   "error": {
	//     "code": 0,
	//     "message": "",
	//     "data": {
	//       "code": 0,
	//       "name": "",
	//       "message": "",
	//       "stack": [
	//         {
	//           "context": {
	//             "level": "",
	//             "file": "",
	//             "line": 0,
	//             "method": "",
	//             "hostname": "",
	//             "thread_name": "",
	//             "timestamp": ""
	//           },
	//           "format": "",
	//           "data": null
	//         }
	//       ]
	//     }
	//   }
	// }
}
