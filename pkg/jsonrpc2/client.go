package jsonrpc2

import (
	"log"
	"time"

	"github.com/gorilla/websocket"
)

type Client struct {
	addr     string
	conn     *websocket.Conn
	ping     time.Duration
	tick     *time.Ticker
	lastPing time.Time
	timeout  time.Duration
	Start    chan struct{}
	Quit     chan struct{}
}

func NewClient() (c *Client) {
	c = &Client{
		ping:    3 * time.Second,
		timeout: 15 * time.Second,
		Start:   make(chan struct{}),
		Quit:    make(chan struct{}),
	}
	go func(c *Client) {
		// wait until start signal or quit
		select {
		case <-c.Quit:
		case <-c.Start:
		}
		c.tick = time.NewTicker(c.ping)
	runOut:
		for {
			select {
			case <-c.Quit:
				break runOut
			default:
				if typ, bytes, err := c.conn.ReadMessage(); err != nil {
					log.Println("failed to read:", err)
				} else {
					if typ == websocket.TextMessage {
						log.Println(string(bytes))
					}
				}
			}
		}
	pingOut:
		for {
			select {
			case <-c.Quit:
				break pingOut
			case <-c.tick.C:
				// do a ping, update last pong time, check for timeout,
				err := c.conn.WriteMessage(websocket.PingMessage, []byte{})
				if err != nil {
					log.Printf("failed to ping: %s", err)
					// TODO: shouldn't this close or re-establish the connection?
				}
			}
		}
	}(c)
	return
}

func (c *Client) SetPing(seconds int) (C *Client) {
	c.ping = time.Duration(seconds) * time.Second
	c.tick = time.NewTicker(c.ping)
	return c
}

func (c *Client) SetTimeout(seconds int) (C *Client) {
	c.timeout = time.Duration(seconds) * time.Second
	return c
}

func (c *Client) Dial(addr string) (err error) {
	c.addr = addr
	conn, _, err := websocket.DefaultDialer.Dial(addr, nil)
	if err != nil {
		return err
	} else {
		c.conn = conn
		// start handler
		close(c.Start)
	}
	return nil
}

func (c *Client) Close() (err error) {
	// close the connection
	if c.conn != nil {
		err = c.conn.Close()
	}
	// stop the handler
	close(c.Quit)
	return
}

