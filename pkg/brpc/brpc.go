// Package brpc provides the JSONRPC2 protocol messaging framework as used by
// steem and derived blockchain node RPCs
package brpc

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

type Request struct {
	JSONRPC  string      `json:"jsonrpc"`
	Method   string      `json:"method"`
	Params   interface{} `json:"params"`
	Id       int64       `json:"id"`
	response interface{}
}

type Response struct {
	Id     int64       `json:"id"`
	Result interface{} `json:"result,omitempty"`
	Error  *struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
		Data    struct {
			Code    int    `json:"code"`
			Name    string `json:"name"`
			Message string `json:"message"`
			Stack   []struct {
				Context struct {
					Level      string `json:"level"`
					File       string `json:"file"`
					Line       int    `json:"line"`
					Method     string `json:"method"`
					Hostname   string `json:"hostname"`
					ThreadName string `json:"thread_name"`
					Timestamp  string `json:"timestamp"` // format: "2017-06-07T02:28:07"
				} `json:"context"`
				Format string      `json:"format"`
				Data   interface{} `json:"data"`
			} `json:"stack"`
		} `json:"data"`
	} `json:"error,omitempty"`
}

// Client
type Client struct {
	conn      *websocket.Conn
	callbacks map[int64]func(response Response, err error)
	lastId    int64
	isVerbose bool
	sync.Mutex
	quit    chan struct{}
	timeout time.Duration
	ping    time.Duration
}

// Create a new client
func NewClient(addr string, pingSeconds, timeoutSeconds int, verbose bool, quit chan struct{}) (*Client,
	error) {
	conn, _, err := websocket.DefaultDialer.Dial(addr, nil)
	if err != nil {
		log.Fatal("failed to dial:", err)
		return nil, err
	}
	newClient := &Client{
		conn:      conn,
		callbacks: make(map[int64]func(Response, error)),
		lastId:    0,
		isVerbose: verbose,
		quit:      quit,
		timeout:   time.Duration(timeoutSeconds) * time.Second,
		ping:      time.Duration(pingSeconds) * time.Second,
	}
	// wait for incoming messages,
	go func(client *Client) {
	out:
		for {
			select {
			case <-client.quit:
				break out
			default:
				if typ, bytes, err := client.conn.ReadMessage(); err != nil {
					log.Println("failed to read:", err)
				} else {
					if typ == websocket.TextMessage {
						if client.isVerbose {
							log.Printf("received bytes: %s", string(bytes))
						}
						var res Response
						if err := json.Unmarshal(bytes, &res); err == nil {
							client.Lock()
							if fn, exists := client.callbacks[res.Id]; exists {
								go fn(res, nil)                  // call callback function
								delete(client.callbacks, res.Id) // delete used callback function
							} else {
								log.Printf("no such callback with id: %d", res.Id)
							}
							client.Unlock()
						} else {
							log.Printf("failed to decode received message: %s", err)
						}
					}
				}
			}
		}
	}(newClient)
	// send ping (for keeping connection)
	go func(client *Client) {
	out:
		for {
			select {
			case <-client.quit:
				break out
			default:
				time.Sleep(client.ping)
				client.Lock()
				err := client.conn.WriteMessage(websocket.PingMessage, []byte{})
				client.Unlock()
				if err != nil {
					log.Printf("failed to ping: %s", err)
					// TODO: shouldn't this close or re-establish the connection?
				}
			}
		}
	}(newClient)
	return newClient, nil
}

// Get a new request
func (c *Client) NewRequest(api, function string, params, response interface{}) Request {
	req := Request{
		JSONRPC:  "2.0",
		Method:   api + "." + function,
		Params:   params,
		Id:       c.lastId,
		response: response,
	}
	c.lastId += 1 // increase last id
	return req
}

// Send request and return its result through callback function
func (c *Client) SendRequestAsync(
	request Request,
	callback func(r Response, e error),
) (err error) {
	var bytes []byte
	if bytes, err = json.Marshal(request); err == nil {
		c.Lock()
		c.callbacks[request.Id] = callback
		err = c.conn.WriteMessage(websocket.TextMessage, bytes)
		c.Unlock()
	}
	return err
}

// Send request and return its result synchronously
func (c *Client) SendRequest(request Request) (r Response, err error) {
	rCh := make(chan Response)
	eCh := make(chan error)
	err = c.SendRequestAsync(
		request,
		func(r Response, e error) {
			if e != nil {
				eCh <- e
			} else {
				rCh <- r
			}
		},
	)
	if err == nil {
		// wait for the callback
		select {
		case r := <-rCh:
			return r, nil
		case e := <-eCh:
			err = e
		case <-time.After(c.timeout): // timeout
			err = fmt.Errorf("request timed out in %d sec", c.timeout)
		}
	}
	return Response{}, err
}

// Close connection
func (c *Client) Close() error {
	c.Lock()
	defer c.Unlock()
	return c.conn.WriteMessage(
		websocket.CloseMessage,
		websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""),
	)
}
