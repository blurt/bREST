// Package main is a short command that generates an output containing
// all method names and the signatures returned by jsonrpc
package main

import (
	"gitlab.com/blurt/bREST/pkg/brpc"
)

var (
	testAPIAddress = "ws://127.0.0.1:8752"
)

var client *brpc.Client

var quit chan struct{}

func init() {
	var err error
	quit = make(chan struct{})
	client, err = brpc.NewClient(testAPIAddress, 3, 15, true, quit)
	if err != nil {
		panic(err)
	}
}

func main() {
	// methods, err := blurtd.GetMethods(client)
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println("[")
	// for i := range methods {
	// 	fmt.Println("{\n\"method\":\"" + methods[i] + "\",\n")
	// 	sig, err := blurtd.GetSignature(client, methods[i])
	// 	if err == nil {
	// 		fmt.Println("\n\"args\":\n", strconv.Quote(spew.Sdump(
	// 			sig["args"]))+",")
	// 		fmt.Println("\"ret\":\n", strconv.Quote(spew.Sdump(
	// 			sig["ret"]))+",")
	// 	}
	// 	fmt.Println("},")
	// }
	// fmt.Println("]")
}
