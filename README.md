# bREST

An RESTful RPC interface for Steem API protocol for humans

[![pipeline status](https://gitlab.com/blurt/bREST/badges/master/pipeline.svg)](https://gitlab.com/blurt/bREST/-/commits/master)
[![coverage report](https://gitlab.com/blurt/bREST/badges/master/coverage.svg)](https://gitlab.com/blurt/bREST/-/commits/master)

bREST is an aggregator and modular query engine for Steem based blockchains,
the back-end data source for the upcoming Blurt mobile app.

bREST uses the Cete interface for dgraph.io's Badger key/value database engine
and will be running a set of 5 global nodes that shard according to a set of
rules relating to post tags for country and language code.

bREST nodes will know how to relay broadcasts of transactions to their 
local region master full RPC blurtd, steemd and hived nodes. 

Picking them up on the other side as they are stored into blocks, each node
will read off all posts that are to be stored in the regional database
tables and the rest stored in the global data which will be also validated
and replicated directly on request from a newly deployed bREST node to
eliminate re-indexing already indexed data. For this also, all 5 regions will
share an administrative user database to control access to this service. It
will then be possible for cache operators, such as witnesses on one or more
of the chains as part of their community service.

bREST will replicate the indexes implemented in the steemd core, pre
-processing and caching all of the data for each account and community. 
Before a node starts to process a new block, however, it will query other
nodes which may have already completed this and directly copy out all the
transactions indices in the new block. Occasionally a node will be lagging
due to transient local network conditions or background processes so if work on
parallelisable processes has been completed, it doesn't have to be repeated.

## bREST architecture

### Regional clusters

There will be 6 clusters, Asia, North America, South America, Africa, Europe, 
Oceania. Each cluster will have a prescribed subset of all of the language
tag and country names appearing in post tags, as appropriate for the region. 
South America will have mainly spanish and portuguese, Europe will have about
20 (english, german, french, portuguese, spanish, dutch, danish, swedish, 
norwegian, finnish, bulgarian, serbian, BiH, Slovenian, Polish, Czech, Slovak, 
Russian, Belarusian, Estonian, Latvian, Ukrainian, Romanian - and all the
rest of course), asia will have chinese, japanese, korean, and so on,
and out of these groups, the country tags will also select to be stored in
that region.
  
For testing, an instance of the shard store will be run to filter each shard.

Possibly in future a web service can be queried to determine post language
for sharding when neither a country nor language code appears in the tags of
a post. The client will automatically add them according to user
configuration of the language of the interface. Thus likely for the Blurt
network, there will never be such a need but other networks may have
interfaces that don't provide useful language metadata.

### Post Storage vs monetary and vote related storage

The actual post content will be stored in a flat file database as 1-64kb
sized blocks of data are most optimally stored by filesystems. The non-post
content will all live in its own database with reference hashes to the flat
file database files and interface fetches them on request. The same goes for
all of the transactions tied to automated payment schedules such as power
downs.

### Search Indexes

Lastly, once the core data is in place, indexes will be made to tie together
votes and monetary transactions on a per-user basis and content tied to
communities and SMTs will have indexes for searching grouped data from these
sets.

These indexes will be built in a semi-modular fashion and if present will be
available to the swagger RPC endpoint. 

### Low Level Stuff

For queries from a user at one bREST cluster, that require data from
another sharded dataset, there is a binary RPC between cluster endpoints
that implements the inter-cluster protocols for forwarding searches to the
engines holding the data. 

The links to full nodes and the links to client nodes both use JSONRPC 2, 
with JSON serialization, but this is not as fast or efficient as a KCP
based, binary (Golang's native Gob format) serialization. 
 
This RPC will include database duplication, directly copying the underlying
filesystem structure after pausing the engine and copying it, as well as
fetching indexes, querying indexes tied to the shard, and simple direct
requests for content with a given hash.

#### Cete RAFT replication

Each regional cluster is a Cete of badger databases... One node acts as the
master and this can change if it goes down, and the master node is the one
that queries the steem based blockchain nodes for new blocks, which it then
ingests and generates new data and stores it, which is then stored on the
slave nodes in the cluster.

#### Binary RPC for auxiliary services

In the future it might be desirable to create specialised servers for
specific purposes. For this, a more tightly coupled KCP/Gob RPC will be
created that return the results of the same queries as done by the main
swagger RESTful interface. This could ultimately be a preferable way to
structure the distribution of processing, nodes connected to their regional
bREST cluster can then become sources of additional information, and
indeed their servers could be queried by pass-through extensions to the
Swagger API while keeping the infrastructure separate.

## Docker Dev Environment

This repository includes a docker image in its registry and it can be run 
to provide a Blurt testnet node for test suites:

`docker run --network=host --expose 8752 -p 8752:8752 -i -t --entrypoint /usr/local/bin/blurtd registry.gitlab.com/blurt/bREST`

